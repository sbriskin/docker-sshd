NAME := sshd
TAG := latest
IMAGE_NAME := sbriskin/$(NAME)

.PHONY: help build push clean run status test kill

help:
	@printf "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' -e 's/^\(.\+\):\(.*\)/\ \1\ :\2/' | column -c2 -t -s :)\n"

build: ## Builds docker image
	docker build --pull -t $(IMAGE_NAME):$(TAG) .
	docker tag $(IMAGE_NAME):$(TAG) $(IMAGE_NAME):latest

push: ## Pushes the docker image to docker.io
	# Don't --pull here, we don't want any last minute upsteam changes
	docker build -t $(IMAGE_NAME):$(TAG) .
	docker tag $(IMAGE_NAME):$(TAG) $(IMAGE_NAME):latest
	docker push $(IMAGE_NAME):$(TAG)
	docker push $(IMAGE_NAME):latest

clean: ## Remove built images
	docker rmi $(IMAGE_NAME):$(TAG)
	docker rmi $(IMAGE_NAME):latest

run: ## Run container
	mkdir `pwd`/.ssh && ssh-keygen -t ed25519 -N '' -f `pwd`/.ssh/id_ed25519
	docker run --name $(NAME) --restart unless-stopped -d -p 2222:22 -v `pwd`/.ssh/id_ed25519.pub:/etc/authorized_keys/user -e SSH_USERS="user:1000:1000" $(IMAGE_NAME):$(TAG)

status: ## Show container status
	docker images | grep $(IMAGE_NAME)
	docker ps -a | grep $(IMAGE_NAME)
	
test: ## Test connection to container
	ssh -v user@localhost -p 2222 -i `pwd`/.ssh/id_ed25519
	ssh-keygen -R [localhost]:2222

kill: ## Stop and remove container
	docker kill $(NAME)
	docker rm $(NAME)
	rm -rf `pwd`/.ssh/
